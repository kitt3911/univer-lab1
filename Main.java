import java.util.Scanner;

public class Main{
    static Scanner in = new Scanner(System.in);
    public static void TabulateFunctionSin(double LeftBorder,double RightBorder,double steps){
        while(LeftBorder<RightBorder){
            System.out.printf("\n x %.5f:", LeftBorder);
            System.out.printf("   Sin(x) :");
            System.out.print(Math.sin(LeftBorder));
            LeftBorder+=steps;
        }
    }
    public static  void less8_9(){
        int array[],N;
        System.out.printf("Press number:");
        N = in.nextInt();
        array = new int[N];
        for(int i=0;i<N;i++){
            array[i]=in.nextInt();
        }
        for(int num: array){
            System.out.print(num + " ");
        }
        System.out.println();
    }
    public static void less10(int arr[]){
        int sum=0;
        for(int num: arr){
            sum+=num;
        }
        System.out.println(sum);
    }
    public static void less11(int arr[]){
        int evenNum=0;
        for(int num: arr){
            if(num%2==0){
                evenNum+=num;
            }
        }
        System.out.println(evenNum);
    }
    public static void less12(int arr[],int a,int b){
        for(int i=a;i<b;i++){
            if(i==arr[i]){
                System.out.println(arr[i]);
            }
        }
    }
    public static void less13(int arr[]){
        boolean positive=true;
        for(int num:arr){
            if(num<0){
                positive=false;
            }
        }
        if(positive==true){
            System.out.println("Yes");
        }
        else{
            System.out.println("No");
        }
    }
    public static void less14(int arr[]){
        for(int i=0;i<arr.length /2;i++){
            int tmp=arr[i];
            arr[i]=arr[arr.length-i-1];
            arr[arr.length-i-1]=tmp;
        }
    }
    public static void main(String[] args){
        less8_9();
    }
}
